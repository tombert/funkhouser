//
//  AppDelegate.h
//  Funkhouser
//
//  Created by Bobby Gammill on 4/15/14.
//  Copyright (c) 2014 Monadic. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface AppDelegate : NSObject <NSApplicationDelegate>

@property (assign) IBOutlet NSWindow *window;

@end
