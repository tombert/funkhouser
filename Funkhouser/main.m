//
//  main.m
//  Funkhouser
//
//  Created by Bobby Gammill on 4/15/14.
//  Copyright (c) 2014 Monadic. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[])
{
    return NSApplicationMain(argc, argv);
}
